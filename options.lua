vim.opt.autochdir = true -- your working dir = file dir
vim.opt.autoread = true -- if a file is changed outside of vim, automatically reload without asking
vim.opt.autowriteall = true -- write on any leaving on buffer
vim.opt.backup = false -- no backup file
vim.opt.clipboard = "unnamedplus" -- Copy Paste between vim and everything else
vim.opt.cmdheight = 3 -- more space in the neovim command line for displaying messages
vim.opt.colorcolumn = "80" -- visual hint where you reach 80 characters
vim.opt.cursorline = true -- highlight current line
vim.opt.expandtab = true -- convert tab to spaces
vim.opt.hidden = true -- When leaving buffer, hide it instead of closing it
vim.opt.history = 1000 -- longer history
vim.opt.ignorecase = true -- while searching undo with \C in search
vim.opt.incsearch = true -- set incremental search, like modern browsers
vim.opt.linebreak = true -- wrap at wordborder
vim.opt.list = true -- show trailing spaces and tabs  see listchars
vim.opt.pumheight = 10 -- height of pop up menu
vim.opt.relativenumber = true -- only current line with absolute line number
vim.opt.scrolloff = 2 -- keep more context when scrolling off the end of a buffer
vim.opt.shiftwidth = 4
vim.opt.showtabline = 2 -- always show tabline at the top
vim.opt.sidescrolloff = 8
vim.opt.smartcase = true -- case-sensitive search if it contains upper-case characters
vim.opt.smartindent = true -- makes indenting smart
vim.opt.spell = false
vim.opt.spelllang = "de" -- ~/.local/share/lunarvim/site/spell/de.utf-8.spl
vim.opt.splitbelow = true -- Horizontal splits will automatically be below
vim.opt.splitright = true -- Vertical splits will automatically be to the right
vim.opt.swapfile = false -- instead of swap use git
vim.opt.tabstop = 4
vim.opt.termguicolors = true
vim.opt.title = true -- Show file title at the top of terminal window
vim.opt.titlestring = "%t%( %M%)%( (%{expand(\"%:~:h\")})%)%( %a%) - LVIM" -- what the title of the window will be set to
vim.opt.wrap = false -- display lines as one long line
