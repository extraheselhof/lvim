vim.api.nvim_create_augroup("myVimAutocmds", {})
vim.api.nvim_create_autocmd("VimResized", {
    command = "wincmd =",
    group = "myVimAutocmds"
})
-- coffee-script
vim.api.nvim_create_autocmd("BufNewFile", {
    pattern = { "*.coffee", "*.litcoffe" },
    callback = function()
        vim.cmd("map <buffer> <space>j :w<CR>:CoffeeRun<CR>")
        vim.cmd("map <buffer> <space>m :w<CR>:make -b<CR>")
    end,
    group = "myVimAutocmds"

})
vim.api.nvim_create_autocmd("Bufread", {
    pattern = { "*.coffee", "*.litcoffe" },
    callback = function()
        vim.cmd("map <buffer> <space>r :w<CR>:CoffeeRun<CR>")
        vim.cmd("map <buffer> <space>m :w<CR>:make -b<CR>")
    end,
    group = "myVimAutocmds"
})

-- markdown-preview
-- tm => toggle the markdown preview
--     let g:markdown_preview_on = 0
--     au! BufWinEnter *.md,*.markdown,*.mdown let g:markdown_preview_on = g:markdown_preview_auto || g:markdown_preview_on
--     au! BufWinLeave *.md,*.markdown,*.mdown let g:markdown_preview_on = !g:markdown_preview_auto && g:markdown_preview_on
--     nmap tm @=(g:markdown_preview_on ? ':Stop' : ':Start')<CR>MarkdownPreview<CR>:let g:markdown_preview_on = 1 - g:markdown_preview_on<CR>
--
--autocmd BufNewFile,BufReadPost *.md setfiletype markdown
--
-- TODO <space>m bei markdown default: :pandoc -o filename.pdf file or bash
-- script or custom command if mentioned in header
