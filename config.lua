vim.cmd('source ~/.config/lvim/statusline.lua')
vim.cmd('source ~/.config/lvim/options.lua')
vim.cmd('source ~/.config/lvim/keybindings.lua')
vim.cmd('source ~/.config/lvim/autocmds.lua')
vim.cmd('source ~/.config/lvim/jab.lua')

-- lvim.builtin.luasnip.active = true
vim.g.loaded_ruby_provider = 0
vim.g.loaded_perl_provider = 0

-- general
lvim.log.level = "warn"
lvim.format_on_save = true
lvim.colorscheme = 'koehler'
-- vim.cmd('source ~/.config/lvim/koehlerjb.vim')
-- to disable icons and use a minimalist setup, uncomment the following
-- lvim.use_icons = false

-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = ","
-- add your own keymapping
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = false

-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {
    "bash",
    "javascript",
    "json",
    "lua",
    "python",
    "typescript",
    "tsx",
    "css",
    "html",
    "yaml",
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enabled = true

-- jab linkfile
function TimeAtEnd()
    vim.cmd("normal! Go")
    local txt = vim.fn.strftime("%Y-%m-%d %H:%M:%S")
    vim.cmd("normal! a" .. txt .. " ")
end

function LinkInsert()
    vim.fn.system("/home/jab/bin/xdotool.sh")
    TimeAtEnd()
    vim.cmd("normal! p")
end

-- Additional Plugins
lvim.plugins = {
    { "kchmck/vim-coffee-script" },
    {
        "iamcco/markdown-preview.nvim",
        run = function() vim.fn["mkdp#util#install"]() end,
        -- run = "cd app && yarn install",
        -- run = "cd app && npm install",
        ft = { "vimwiki", "md", "markdown", "litcoffee", "pandoc" },
    },
    { "vimwiki/vimwiki" },
    { "ibhagwan/fzf-lua" },
    { "norcalli/nvim-colorizer.lua" },
    { "unblevable/quick-scope" },
    { "jamessan/vim-gnupg" },
    { "vim-pandoc/vim-pandoc" },
    { "vim-pandoc/vim-pandoc-syntax" },
    -- { "ActivityWatch/aw-watcher-vim" }
    -- { "SirVer/ultisnips" }
    -- {"szw/vim-g"}, -- google-search  :Google Begriff
}


-- from here on, settings for loaded plugins


-- markdown-preview
-- vim.g.mkdp_auto_start = 1
vim.g.mkdp_auto_close = 0 -- don't close preview automatically
vim.g.mkdp_filetypes = { 'litcoffee', 'markdown', 'pandoc', 'vimwiki', 'md' }

-- vimwiki
vim.g.vimwiki_list = { { syntax = 'markdown', ext = '.md' } }
vim.g.vimwiki_list = { { path = '/qsx/intra/2020/vimwiki', syntax = 'markdown', ext = '.md' } }

-- fzf-lua (telescope works for most of functionality but i'm used to fzf)
require 'fzf-lua'.setup {
    fzf_bin = '/home/jab/.fzf/bin/fzf',
}

-- colorizer
require("colorizer").setup({ "css", "scss", "html", "javascript", "coffee", "litcoffee", "vim", "lua" }, {
    RGB = true, -- #RGB hex codes
    RRGGBB = true, -- #RRGGBB hex codes
    RRGGBBAA = true, -- #RRGGBBAA hex codes
    rgb_fn = true, -- CSS rgb() and rgba() functions
    hsl_fn = true, -- CSS hsl() and hsla() functions
    css = true, -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
    css_fn = true, -- Enable all CSS *functions*: rgb_fn, hsl_fn
})

-- quick-scope
-- Trigger a highlight in the appropriate direction when pressing these keys:
vim.g.qs_highlight_on_keys = { 'f', 'F', 't', 'T' }
vim.cmd("highlight QuickScopePrimary guifg='#00C7DF' gui=underline ctermfg=155 cterm=underline")
vim.cmd("highlight QuickScopeSecondary guifg='#afff5f' gui=underline ctermfg=81 cterm=underline")
vim.g.qs_max_chars = 150

-- vim-pandoc
-- vim.g.pandoc_command_autoexec_on_writes = 1
-- vim.g.pandoc_command_autoexec_command = "Pandoc! pdf"

-- INSPIRATION for jab (for init.vim)
-----------------------------
-- chrisatmachine.com
-- https://github.com/garybernhardt/dotfiles/blob/main/.vimrc
-- https://github.com/WaylonWalker/devtainer/blob/main/nvim/.config/nvim/keymap.vim
-- https://github.com/ThePrimeagen/.dotfiles/blob/master/nvim/.config/nvim/init.vim
--
-- https://github.com/nicknisi/dotfiles/blob/main/config/nvim/init.lua
-- https://github.com/zenbro/dotfiles/blob/master/.nvimrc#L151-L187
--
-- https://github.com/euclio/vimrc/blob/master/vimrc
-- https://github.com/tpope/tpope/blob/master/.vimrc
-- https://github.com/lukas-reineke/dotfiles/blob/master/vim/lua/mappings.lua
-- https://github.com/junegunn/dotfiles/blob/master/vimrc
