local pickers = require "telescope.pickers"
local finders = require "telescope.finders"
local conf = require("telescope.config").values
local actions = require "telescope.actions"
local action_state = require "telescope.actions.state"
local make_entry = require "telescope.make_entry"

-- some variables
local Jab_logfname = '/home/' .. vim.fn.expand('$USER') .. '/vim.log' -- eg /home/jab/lvim.log
-- let g:Jb_logfname = get(g:, 'Jb_logfname', '/home/'.$USER.'/vim.log') "eg /home/jb/vim.log
local Jab_browser = 'brave'

-- some helper functions

local function JabLeft(str, needle) -- JabLeft('HelloWorld', 'W') -> 'Hello'
    if str:find(needle) == nil then
        return str
    else
        local left = str:find(needle) - 1
        return str:sub(1, left)
    end
end

local function JabRight(str, needle) -- JabLeft('HelloWorld', 'W') -> 'orld'
    if str:find(needle) == nil then
        return str
    else
        local right = str:find(needle) + needle:len()
        return str:sub(right, -1)
    end
end

local function JabExpandArg(arg, expand) -- replace argument, if missing
    -- this is used, when calling a function with an empty argument or '.' as
    -- arg. The argument is replaced by the <cword>, <cWORD>, <cfile> or with
    -- the current line when expand is '<cline>'.
    -- eg JabExpandArg('', '<cword>') returns the word at the cursor position.
    -- This helps to programm a shortcut.
    if arg == '' or arg == '.' then
        if expand == '<cline>' then
            arg = vim.fn.getline('.')
        else
            arg = vim.fn.expand(expand)
        end
    end
    return arg
end

local function JabUniq(arr) -- uniq of arr, even if it's not sorted
    local hash = {}
    local uniqarr = {}
    for _, v in ipairs(arr) do
        if not hash[v] then
            uniqarr[#uniqarr + 1] = v
            hash[v] = true
        end
    end
    return uniqarr
end

local function JabFileFilter(fname, needle) -- read file, but only lines containing needle
    local lines = vim.fn.readfile(fname)
    local res = {}
    for _, v in ipairs(lines) do
        if v:find(needle) ~= nil then
            res[#res + 1] = v
        end
    end
    return res
end

-- logic start -----------------------------------------------------------------

function JabVimlogWrite() -- write current filename to permanent logfile
    -- We need this file as source where to grep for information
    local txt = { vim.fn.hostname() .. vim.fn.strftime(" %Y-%m-%d %H:%M:%S ") .. vim.fn.expand('%:p') }
    vim.fn.writefile(txt, Jab_logfname, 'a')
end

vim.api.nvim_create_augroup("vimlog", {})
vim.api.nvim_create_autocmd("BufWritePost", {
    command = "lua JabVimlogWrite()",
    group = "vimlog"
})

local function JabOneLog(logfname, needle) -- array of recent saved files
    local res = {}
    for _, v in ipairs(JabFileFilter(logfname, needle)) do
        res[#res + 1] = '/' .. JabRight(v, "/") -- only the filename in the line
    end
    res = vim.fn.reverse(res) -- newest files at start
    local uniqarr = JabUniq(res)
    return uniqarr
end

function JabLineUrl(txt) -- extract or compose url from cline or argument
    -- :lua JabLineUrl('heise.de') -> https://heise.de
    -- :lua JabLineUrl('[heise](https://heise.de)' -> https://heise.de
    local url = JabExpandArg(txt, '<cline>')
    if url:find('http') ~= nil then
        url = JabRight(url, '://')
        if url:find('[%s%)%]]') then
            -- url = JabLeft(url, '[%s%)%]]')
            url = JabLeft(url, '[^%a%d%.#/%?=_%-]')
        end
    end
    return 'https://' .. url
end

function JabBrows(url) -- open url in browser
    -- open browser with url as the argument or the current line if url is '.' or ''
    -- :lua JabBrows https://heise.de
    -- :lua JabBrows heise.de           # opens https://heise.de
    -- :lua JabBrows [heise](https://heise.de)
    -- :lua JabBrows . #  opens the browser with the current line as link starting from http...
    local cmd = "!" .. Jab_browser .. " '" .. JabLineUrl(url) .. "'"
    vim.cmd('echo "' .. cmd .. '"')
    vim.cmd('silent :' .. cmd)
end

local function JabTelescope(fileArr, needle, addArgs, opts)
    local vimgrep_arguments = { "rg", "--color=never", "--no-heading", "--line-number", "--column", "--smart-case" }
    opts.cwd = opts.cwd and vim.fn.expand(opts.cwd) or vim.loop.cwd()

    local filelist = {}
    for _, path in ipairs(fileArr) do
        table.insert(filelist, vim.fn.expand(path))
    end

    local additional_args = addArgs -- {}
    table.insert(vimgrep_arguments, addArgs) -- --max-count=1 find only first line, if filenames are searched

    opts.entry_maker = opts.entry_maker or make_entry.gen_from_vimgrep(opts)
    local args = vim.tbl_flatten {
        vimgrep_arguments,
        additional_args,
        "--",
        needle,
        "--files",
        filelist,
    }

    pickers.new(opts, {
        prompt_title = "JabGrep",
        finder = finders.new_oneshot_job(args, opts),
        previewer = conf.grep_previewer(opts),
        -- sorter = sorters.highlighter_only(opts),
        -- sorter = conf.file_sorter(opts),
        sorter = conf.generic_sorter(opts),
    }):find()
end

function JabFzCoffee(fileAndAnchor) -- show coffee definitions
    local search = JabExpandArg(fileAndAnchor, '<cfile>')
    search = 'coffee#' .. search .. ".*=.\\("
    vim.cmd('echo "' .. search .. '"')
    JabFzEdit(search)
end

-- :lua JabFzCoffee("fromTo")  shows all coffeedefinitions containing "fromTo"
-- :lua JabFzCoffee(".") shows all coffeedefinitions containing cursor word

function JabFzEdit(fileAndAnchor) -- edit first matching file in vimlog (and find #anchor)
    -- :Je night#alpenglow   grep "alpenglow" in files containing "night"
    -- :Je .#alpenglow       grep "alpenglow" in all edited files
    -- :Je alpenglow         grep "alpenglow" in all edited files
    -- :Je nightwish.md      search for files with names containing "nightwish.md"
    -- :Je .#nightwish.md    search for "nightwish.md" in all edited files
    -- :Je .                 use cursorword als search element as in the lines above
    -- :Je readme.           fzf filenames containing readme , show begin of file
    -- :Je This@@would       grep "this would" in all editfiles
    fileAndAnchor = JabExpandArg(fileAndAnchor, '<cfile>')
    local fileneedle = ''
    local needle = fileAndAnchor
    if fileAndAnchor:find('#') == nil then
        if needle:find('.') ~= nil then -- search for filename
            fileneedle = needle
            needle = '.'
        end
    else
        fileneedle = JabLeft(fileAndAnchor, '#')
        needle = JabRight(fileAndAnchor, '#')
    end
    local fileArr = JabOneLog(Jab_logfname, fileneedle) -- all files with a name that includes fileneedle
    if fileArr[1] == nil then -- no file matched
        fileArr = JabOneLog(Jab_logfname, '') -- search in all files
        if needle == "." then
            needle = fileneedle
        end
    end
    needle = string.gsub(needle, '@@', ' ')
    local maxRows = {} -- all lines
    if needle == '.' then
        table.insert(maxRows, "--max-count=1") -- rg match only first line in file
    end
    JabTelescope(fileArr, needle, maxRows, {})
end

function JabFzLink(needle) -- list markdown-links
    -- :Jl junegunn   list markdown links containing "junegunn"
    -- :Jl .          list markdown links containing the cursorword
    needle = JabExpandArg(needle, '<cfile>')
    local fileneedle = '.'
    if needle:find('#') ~= nil then
        fileneedle = JabLeft(needle, '#')
    end
    needle = JabRight(needle, '#')
    needle = [[]\(h.*]] .. needle .. '|' .. needle .. [[]\(h]] -- needle and md-link
    local fileArr = JabOneLog(Jab_logfname, fileneedle)
    local attach_mappings = function(prompt_bufnr, map)
        actions.select_default:replace(function()
            actions.close(prompt_bufnr)
            local selection = action_state.get_selected_entry()
            -- print(vim.inspect(selection))
            vim.cmd('lua JabBrows("' .. selection['text'] .. '")')
        end)
        return true
    end
    JabTelescope(fileArr, needle, {}, { attach_mappings = attach_mappings })
end

vim.cmd('command! -nargs=1 Jb lua JabBrows(<f-args>)')
vim.cmd('command! -nargs=1 Je lua JabFzEdit(<f-args>)')
vim.cmd('command! -nargs=1 Jl lua JabFzLink(<f-args>)')
-- :Jl Peterson    show list of markdown links with Peterson and open in Browser
-- :Je vetapi      all files, containing 'vetapi'
-- :Je qlib#fromTo grep in 'qlib'-files for 'fromTo'
-- :Je .#notify    grep 'notify' in all files

-- # Bookmarks for edited files
--
-- vim.log gets a line for every writing of a file
--
-- JbEdit uses the vim.log to find matching files and fzf to find text inside those
-- files. You type ":Je init.vim#fzf" and you will be shown your vimrc and the
-- positions of the string fzf.
--
-- You could also write a bookmark in an arbitrary file and link to "init.vim#fzf"
-- by putting your cursor on that text and start the function with ":Je ."
-- or <space>e
--
-- The bookmark has two parts. The first part is a search expression for the file
-- The second part is the text, that is search in these files. If you use a dot for
-- one part, it is a wild card for all. ".#matrix" searches for matrix in all the
-- files. "matrix#." lists the first line of all files. You can omit the following
-- "#.". "matrix" shows all files containing matrix in filename or path.
--
-- The workflow is similar to google the web, exept the database are all your edited
-- files.
--
-- I don't use the shada file of vim with the history of all visited files,
-- because this also has all files I only have read.
-- Maybe this could be an extra function. I would increase the amount of remembered
-- files with "set viminfo='100,<50,s10,h" to get a list of theses files in
-- vimscript you can use v:oldfiles.
--
-- # Bookmarks for WWW in markdown.
--
-- The plugin assumes, you put all interesting Internetsites in a Markdownfile
-- You type ":Jl junegunn" and will see all your bookmarks, that contain junegunn
-- in the url or the text to this url on the same line. With <cr> you start
-- browsing this website.
--
-- # Dependencies
--
-- nvim plugin .#telescope
-- shell programm .#ripgrep

function JbCoffee(cmd, such) --  call coffee programm. Use output as vimscript
    -- the coffee programm takes a cmd as first and a second parameter eg  searching
    local cmd = "vimdo.coffee " .. cmd .. " " .. such
    local res = vim.fn.system(cmd)
    vim.cmd(res)
end

vim.cmd("command! -nargs=1 Jo lua JbCoffee('blog','')")
-- command! -nargs=1 Jo call JbCoffee('blog','')
vim.api.nvim_set_keymap('n', ',jo', ":lua JbCoffee('blog','')<CR>", { desc = "Tagesblog aufrufen" })
