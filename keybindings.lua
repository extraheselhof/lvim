function FindFold()
    vim.opt.foldmethod = 'expr'
    vim.opt.foldlevel = 0
    vim.opt.foldcolumn = '2'
    local suchStr = vim.fn.getreg('/') -- substitute(escape(@/, '\\/.*$^~[]'), "[ \t\n]\\+", "\\\\_s\\\\+", "g")
    vim.opt.foldexpr = "(getline(v:lnum)!~'" .. suchStr .. "')"
end

local jbnmap = [[
key      | name                      |  cmd
---      | ----                      |  ---
z_       | fold paragraph            |:set foldexpr=getline(v:lnum)=~'^\\s*$'&&getline(v:lnum+1)=~'\\S'?'<1':1<CR>:set fdm=expr<CR><CR>zM
z/       | fold search word          |:lua FindFold()<cr>zM
I        | Buffer Cycle prev         |:BufferLineCyclePrev
>        | Bufferline Move next      |:BufferLineMoveNext
<        | Bufferline Move prev      |:BufferLineMovePrev
_bg      | Bufferline pick           |:BufferLinePick
_1       | Bufferline Go 1           |:BufferLineGoTo 1
<c-up>   | grow 5 horizontal         |:resize +5
<c-down> | shrink 5 horizontal       |:resize -5
<c-left> | grow 5 vertical           |:vertical resize +5
<c-right>| shrink 5 vertical         |:vertical resize -5
qq       | faster quit               |:wq
0        | swap 0/^ first non white  |^
^        | first col                 |0
_h       | MarkdownPreview           |<Plug>MarkdownPreview
_o       | close other               |:only
_I       | Split vertical            |:vsp
_w       | write update              |:update
_n       | remove highlights         |:nohls
_u       | Recent used files         |:Telescope oldfiles
_f       | find files                |:Telescope find_files
_p       | preview cfile             |:ped +1 <cfile><cr>
_P       | preview close             |:pc
_si      | grep in buffer            |:Telescope current_buffer_fuzzy_find
_*       | grep cword in             |:Telescope grep_string
_sb      | all Telescope func        |:Telescope
_i       | Dashboard startify        |:Alpha
_v       | keybindings               |:vsplit /home/jb/.config/lvim/keybindings.lua
_c       | Farbschema anpassen       |:lua JbColor()
E        | next Buffer               |:BufferLineCycleNext
_U       | Linkpreview to file       |:JbLinkpreview
gX       | open next link Browser    |/http<cr>gx
RR       | Reload Lvim               |:LvimReload
]]

local othermaps = [[
mode | key | name       | cmd
---  |-----|------      |-----
i    |jj   | jescape    |<esc><right>
v    |v    | line mode  |<esc>V
v    |c    | block mode |<esc><c-v>
]]

function ReadMarkdownTable(text)
    local arr = Split(text, '\n') -- zeilen als array
    local title = Split(table.remove(arr, 1):gsub("%s", ''), '|')
    table.remove(arr, 1) --remove separator line
    local lineA = {}
    for _, line in ipairs(arr) do
        if line:len() < 5 then
            break
        end
        local row = {}
        local valA = Split(line:gsub("%s*|", '|'), '|')
        for i, val in ipairs(valA) do
            row[title[i]] = val
        end
        lineA[#lineA + 1] = row
    end
    return lineA
end

function JbMap(jbmap)
    local lineA = ReadMarkdownTable(jbmap)
    for _, line in ipairs(lineA) do
        local cmd = line.cmd
        if cmd:sub(1, 1) == ':' then
            if not cmd:find('<') then
                cmd = cmd .. '<cr>'
            end
        end
        local taste = line.key:gsub('_', '<space>')
        local lmode = 'n'
        if line.mode then
            lmode = line.mode
        end
        vim.keymap.set(lmode, taste, cmd, { noremap = true, silent = true, desc = line.name .. ' jb' })
    end
end

JbMap(jbnmap)
JbMap(othermaps)

function JbLinkpreview(url) -- short information to an url, sometimes with image
    local lurl = JabLineUrl(url) -- if url is empty, use link in current line
    vim.cmd('echo "' .. lurl .. '"')
    local cmd = "coffee /qsx/intra/2020/node/linkpreview.coffee '" .. lurl .. "'"
    local out = vim.fn.system(cmd)
    vim.cmd('echo "view /tmp/linkpreview.txt"')
    -- test _U https://www.youtube.com/watch?v=1-Zl9o7I4Fo
end

function JbColor() -- set custom colors, I did not find out, how to make a colorscheme 2022-12-27 10:12
    vim.cmd [[
        hi comment guifg=green
        hi LineNr  guifg=#444444
    ]]
end

vim.cmd [[
    iab CO2 CO₂
    iab jdt <C-R>=strftime("%Y-%m-%d %H:%M ")<cr><esc>:"Insert YYYY-mm-dd HH:MM"<esc>a
    iab jd <C-R>=strftime("%Y-%m-%d")<CR><esc>:"Insert YYYY-mm-dd"<esc>a
    command! JbLinkpreview :lua JbLinkpreview('')
    " fold   zn:open all folds
    nmap z1 z<space>
    " fold sections  see first line after two empty lines
    nmap z2 :set foldexpr=getline(v:lnum)=~'^\\s*$'&&getline(v:lnum+1)=~'^\\s*$'&&getline(v:lnum+2)=~'\\S'?'<1':1<CR>:set fdm=expr<CR>zM<CR>
    nmap z7 z/
    " fold paragraphs but open first and last for info
    nmap zp z1zMggzoGzo
    " fold sections but open first and last for info
    nmap zP z2zMggzoGzo
    nmap ö :
    " show difference of edited file with the saved one
    function! s:DiffWithSaved()
        let filetype=&ft
        diffthis
        vnew | r # | normal 1Gdd
        diffthis
        exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
    endfunction
    com! Diff call s:DiffWithSaved()
    function! s:KillTrail()
       exe "%s/ *$//"
       exe ":nohls"
    endfunction
    com! KillTrail call s:KillTrail()
    lua JbColor()
]]


-- Explanations for builtin commands
