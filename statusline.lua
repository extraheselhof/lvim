local components = require "lvim.core.lualine.components"

local function condFiletype()
    ---    elseif vim.bo.filetype == 'vimwiki' then
    if vim.fn.expand '%:e' == 'md' then
        return true
    elseif vim.fn.expand '%:e' == 'txt' then
        return true
    else
        return false
    end
end

--- @param trunc_width number trunctates component when screen width is less then trunc_width
--- @param trunc_len number truncates component to trunc_len number of chars
--- @param hide_width number hides component when window width is smaller then hide_width
--- return function that can format the component accordingly
local function trunc(trunc_width, trunc_len, hide_width)
    return function(str)
        local win_width = vim.fn.winwidth(0)
        if hide_width and win_width < hide_width then return ''
        elseif trunc_width and trunc_len and win_width < trunc_width and #str > trunc_len then
            return str:sub(1, trunc_len) .. ('...')
        end
        return str
    end
end

local function isSmall()
    if vim.fn.winwidth(0) < 40 then
        return true
    else
        return false
    end
end

lvim.builtin.lualine.options = {
    section_separators = { right = "" },
}

lvim.builtin.lualine.sections = {
    lualine_x = { { '%{wordcount().words} words', cond = condFiletype, fmt = trunc(100, 1, 100) },
        components.diagnostics,
        components.treesitter,
    },
    lualine_y = { { 'filetype', fmt = trunc(60, 1, 60) } },
    lualine_z = { { '%3p%% ☰ %3l/%{nvim_buf_line_count(0)} ln : %-2v', fmt = trunc(40, 1, 40) },
        { 'location', cond = isSmall } }
}

-- lvim.builtin.lualine.tabline = {
--   lualine_a = { { 'buffers', mode = 2 } },
--   lualine_b = {},
--   lualine_c = { 'filename' },
--   lualine_x = {},
--   lualine_y = {},
--   lualine_z = { 'tabs' }
-- }

-- old vim configuration

-- lua << END
-- local function condFiletype()
-- ---    elseif vim.bo.filetype == 'vimwiki' then
--     if vim.fn.expand '%:e' == 'md' then
--         return true
--     elseif vim.fn.expand '%:e' == 'txt' then
--         return true
--     else
--         return false
--     end
-- end
-- --- @param trunc_width number trunctates component when screen width is less then trunc_width
-- --- @param trunc_len number truncates component to trunc_len number of chars
-- --- @param hide_width number hides component when window width is smaller then hide_width
-- --- @param no_ellipsis boolean whether to disable adding '...' at end after truncation
-- --- return function that can format the component accordingly
-- local function trunc(trunc_width, trunc_len, hide_width, no_ellipsis)
--   return function(str)
--     local win_width = vim.fn.winwidth(0)
--     if hide_width and win_width < hide_width then return ''
--     elseif trunc_width and trunc_len and win_width < trunc_width and #str > trunc_len then
--        return str:sub(1, trunc_len) .. (no_ellipsis and '' or '...')
--     end
--     return str
--   end
-- end
-- local function isSmall()
--     if vim.fn.winwidth(0) < 40 then
--         return true
--     else
--         return false
--     end
-- end
-- require'lualine'.setup {
--     options = {theme = 'onedark'},
--     sections = {
--         lualine_a = {{'mode', fmt=trunc(80, 1, nil, true)}},
--         lualine_b = {{'branch', fmt=trunc(80, 1, 80)}, {'diff', fmt=trunc(80, 1, 80)}, {'diagnostics', fmt=trunc(80, 1, 80)}},
--         lualine_x = {{'%{wordcount().words} words', cond=condFiletype, fmt=trunc(100, 1, 100)}, {'encoding', fmt=trunc(100, 1, 100)}, {'fileformat', fmt=trunc(100, 1, 100)}},
--         lualine_y = {{'filetype', fmt=trunc(60, 1, 60)}},
--         lualine_z = {{'%3p%% ☰ %3l/%{nvim_buf_line_count(0)} ln : %-2v', fmt=trunc(40,1,40)}, {'location', cond=isSmall}}
--     },
--     tabline = {
--         lualine_a = {{'buffers', mode=2}},
--         lualine_b = {},
--         lualine_c = {'filename'},
--         lualine_x = {},
--         lualine_y = {},
--         lualine_z = {'tabs'}
--     }
-- }
-- END
